import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import static org.testng.Assert.*;

public class UnitTests {
    FileHandler fileHandler;

    @BeforeClass
    public void setup() {
        String studentsSample = "./samples/students.txt";
        String questionsSample = "./samples/questions.txt";
        this.fileHandler = new FileHandler(studentsSample, questionsSample);
    }
    @Test
    public void testFileReading() throws IOException {
        List<String> students = fileHandler.getStudentsList();
        List<String> questions = fileHandler.getQuestionsList();
        System.out.println(System.getProperty("user.dir"));

        assertTrue(students.size() > 0, "Ошибка при чтении входных данных (ученики).");
        assertTrue(questions.size() > 0, "Ошибка при чтении входных данных (вопросы).");
    }
    @Test
    public void testOutputFilenameFormat() throws IOException {
        fileHandler.writeResults("Привет из unit-теста!");

        String expectedFilename = new SimpleDateFormat("dd.MM.yyyy").format(new Date()) + ".txt";
        assertTrue(Files.exists(Paths.get(expectedFilename)), "Имя выходного файла неверное.");
    }
    @Test
    public void testFileExistenceCheck() {
        assertFalse(FileHandler.checkFileExists("incorrect path !@#$"), "Проверка существования файла выдает неверный результат (считает несуществующий файл - существующим)");
        assertTrue(FileHandler.checkFileExists("pom.xml"), "Проверка существования файла выдает неверный результат (считает существующий файл - несуществующим)");
    }
}
