import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.print("Введите, пожалуйста, имя файла со списком учеников курса: ");
        String studentsFilename = UserInputHandler.getFilename();
        while (!FileHandler.checkFileExists(studentsFilename)) {
            System.out.print("Такого файла нет. Попробуйте еще раз: ");
            studentsFilename = UserInputHandler.getFilename();
        }
        System.out.print("Введите, пожалуйста, имя файла со списком вопросов: ");
        String questionsFilename = UserInputHandler.getFilename();
        while (!FileHandler.checkFileExists(questionsFilename)) {
            System.out.print("Такого файла нет. Попробуйте еще раз: ");
            questionsFilename = UserInputHandler.getFilename();
        }

        FileHandler fileHandler = new FileHandler(studentsFilename, questionsFilename);
        List<String> studentsList = fileHandler.getStudentsList();
        List<String> questionsList = fileHandler.getQuestionsList();

        if (studentsList.size() > questionsList.size()) {
            System.out.println("Ошибка! Вопросов слишком мало. Так кто-то из учеников останется без внимания.");
        } else {
            Collections.shuffle(studentsList);
            Collections.shuffle(questionsList);

            String results = "";
            for (int i = 0; i < studentsList.size(); i++) {
                results += studentsList.get(i) + " - " + questionsList.get(i) + "\r\n";
            }

            fileHandler.writeResults(results);
            System.out.println("Сгенерированные случайным образом пары ученик-вопрос записаны в файл.");
        }
    }
}
