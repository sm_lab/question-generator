import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FileHandler {
    public String studentsFile;
    public String questionsFile;

    public FileHandler(String studentsFile, String questionsFile) {
        this.studentsFile = studentsFile;
        this.questionsFile = questionsFile;
    }

    public List<String> getStudentsList() throws IOException {
        return Files.readAllLines(Paths.get(studentsFile));
    }

    public List<String> getQuestionsList() throws IOException {
        return Files.readAllLines(Paths.get(questionsFile));
    }

    public void writeResults(String data) throws IOException {
        String filename = new SimpleDateFormat("dd.MM.yyyy").format(new Date()) + ".txt";

        Files.writeString(Paths.get(filename), data);
    }

    public static boolean checkFileExists(String filename) {
        return Files.exists(Paths.get(filename));
    }
}
