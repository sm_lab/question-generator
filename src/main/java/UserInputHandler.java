import java.util.Scanner;

public class UserInputHandler {
    public static String getFilename() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
